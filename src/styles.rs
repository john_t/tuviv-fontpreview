use tuviv::{Color, Modifier, Style};

const fn default_style() -> Style {
    Style {
        fg: None,
        bg: None,
        add_modifier: Modifier::NONE,
        sub_modifier: Modifier::NONE,
    }
}

pub const SIDEBAR: Style = Style {
    fg: Some(Color::Black),
    bg: Some(Color::Rgb(0xf0, 0xe7, 0xdf)),
    ..default_style()
};
pub const SIDEBAR_SELECTED: Style = Style {
    fg: Some(Color::Yellow),
    bg: Some(Color::Rgb(0xf0, 0xe7, 0xdf)),
    ..default_style()
};
