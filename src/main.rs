use std::{
    cmp::Reverse,
    fs,
    io::{Cursor, Read as _},
    num::NonZeroU32,
    path::Path,
    process::Stdio,
};

use fuzzy_matcher::{skim::SkimMatcherV2, FuzzyMatcher};
use tokio::{process::Command, sync::mpsc::UnboundedSender, task::spawn_blocking};

use anyhow::{anyhow, Result};
use crossterm::event::{Event, EventStream, KeyCode};
use fontconfig::{FontFormat, Fontconfig, Pattern};
use image::{imageops::FilterType, DynamicImage};
use tokio::sync::mpsc::unbounded_channel;
use tokio_stream::StreamExt as _;
use tuviv::{
    le::{grid::Sizing, Orientation},
    prelude::{StylableExt, WidgetExt},
    widgets::{Flexbox, Grid, KittyImage, Paragraph},
    App, Color, Widget,
};

mod styles;

const TEXT: &str = "ABCDEFGHIJKLM\nNOPQRSTUVWXYZ\nabcdefghijklm\nnopqrstuvwxyz\n1234567890\n!@#$^&;:\n_-=+'\"|\\(){}[]*";

pub struct PatternWithImage<'a, 'b> {
    pub pattern: Pattern<'a>,
    pub image: Option<KittyImage<'b, DynamicImage>>,
}

const FORMATS: &[FontFormat] = &[FontFormat::TrueType, FontFormat::CFF];

#[tokio::main]
async fn main() -> Result<()> {
    let fc = Fontconfig::new().unwrap();
    let list = fontconfig::list_fonts(&Pattern::new(&fc), None);
    let mut list: Vec<_> = list
        .iter()
        .filter(|x| {
            x.format()
                .ok()
                .map(|x| FORMATS.contains(&x))
                .unwrap_or_default()
        })
        .filter(|x| x.name().map(|x| !x.is_empty()).unwrap_or_default())
        .map(|x| PatternWithImage {
            pattern: x,
            image: None,
        })
        .collect();
    list.sort_by(|a, b| a.pattern.name().cmp(&b.pattern.name()));
    list.dedup_by(|a, b| a.pattern.name() == b.pattern.name());
    let mut pos_in_list: usize = 0;
    let mut search = String::new();
    let mut event_stream = EventStream::new();

    let mut app = App::new()?;

    let mut event = None;

    let matcher = SkimMatcherV2::default();

    let (sender, mut recv) = unbounded_channel();

    loop {
        if let Some(event) = event {
            let old_pos = pos_in_list;
            if let Event::Key(key) = event {
                match key.code {
                    KeyCode::Esc => break,
                    KeyCode::Char(n) => {
                        search.push(n);
                        fuzzy_sort_list(&mut list, &search, &matcher);
                    }
                    KeyCode::Backspace => {
                        search.pop();
                        fuzzy_sort_list(&mut list, &search, &matcher);
                    }
                    KeyCode::Up => {
                        pos_in_list = pos_in_list.saturating_sub(1);
                    }
                    KeyCode::Down => {
                        pos_in_list += 1;
                        pos_in_list = pos_in_list.min(list.len().saturating_sub(1));
                    }
                    _ => (),
                }
            }
            if old_pos != pos_in_list {
                spawn_kitty_image(pos_in_list, list.get(pos_in_list).unwrap(), sender.clone());
            }
        }

        let mut flex_list = Flexbox::new(Orientation::Vertical, false);
        'list: for (i, l) in list.iter().enumerate() {
            if matcher
                .fuzzy_match(l.pattern.name().unwrap_or_default(), &search)
                .is_none()
            {
                break 'list;
            }

            let style = if i == pos_in_list {
                styles::SIDEBAR_SELECTED
            } else {
                styles::SIDEBAR
            };

            flex_list.children.push(
                Paragraph::label(l.pattern.name().unwrap_or("").to_string().with_style(style))
                    .to_flex_child(),
            );
        }
        let flex_list = flex_list.stacked_background(styles::SIDEBAR.bg);

        let selection = list.get(pos_in_list).unwrap();
        // let image = create_kitty_widget(selection.filename().unwrap(), TEXT)?.centered();

        let mut grid = Grid::new()
            .template_columns(vec![Sizing::Auto, Sizing::Fractional(1)])
            .template_row(Sizing::Fractional(1))
            .template_row(Sizing::Auto)
            .child(flex_list.to_scroll().to_grid_child().column(0).row(0))
            .child(
                Paragraph::label(search.as_str().styled())
                    .to_grid_child()
                    .column(0)
                    .row(1)
                    .column_span(2),
            );
        if let Some(image) = &selection.image {
            grid.children.push(image.to_grid_child().column(1).row(0));
        }

        app.render_widget(grid)?;
        event = tokio::select! {
            v = event_stream.next() => v,
            v = recv.recv() => {
                if let Some(v) = v {
                    list.get_mut(v.0).unwrap().image = Some(v.1);
                }
                None
            }

        }
        .transpose()?;
    }

    Ok(())
}

fn fuzzy_sort_list<'a, 'b>(
    list: &mut [PatternWithImage<'a, 'b>],
    search: &str,
    matcher: &impl FuzzyMatcher,
) {
    list.sort_by(|a, b| {
        (
            Reverse((matcher.fuzzy_match(a.pattern.name().unwrap_or_default(), search))),
            a.pattern.name(),
        )
            .cmp(&(
                Reverse((matcher.fuzzy_match(b.pattern.name().unwrap_or_default(), search))),
                b.pattern.name(),
            ))
    })
}

fn spawn_kitty_image(
    i: usize,
    pattern: &PatternWithImage,
    sender: UnboundedSender<(usize, KittyImage<'static, DynamicImage>)>,
) {
    if pattern.image.is_none() {
        let name = pattern.pattern.filename().unwrap_or("").to_owned();
        tokio::spawn(async move {
            let img = create_kitty_widget(&name, TEXT).await.unwrap();
            sender.send((i, img)).unwrap();
        });
    }
}

async fn create_kitty_widget(
    font_path: &str,
    text: &str,
) -> anyhow::Result<KittyImage<'static, DynamicImage>> {
    let cmd = Command::new("convert")
        .args([
            "-size",
            "1024x1024",
            "xc:#FFFFFF00",
            "-fill",
            "#635240",
            "-pointsize",
            "72",
            "-font",
            font_path,
            "-gravity",
            "Center",
            "-annotate",
            "+50+0",
            text,
            "png:-",
        ])
        .stdout(Stdio::piped())
        .output()
        .await?;
    let stdout = Cursor::new(cmd.stdout);

    let image =
        spawn_blocking(|| image::io::Reader::with_format(stdout, image::ImageFormat::Png).decode())
            .await??;

    Ok(KittyImage::new(image, NonZeroU32::new(1).unwrap())?.resize_filter(FilterType::Lanczos3))
}
